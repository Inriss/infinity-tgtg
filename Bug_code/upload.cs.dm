
/obj/item/weapon/storage/belt/bandolier
	name = "bandolier"
	desc = "A bandolier designed to hold up to eight shotgun shells."
	icon = 'Bug_code/sprites.dmi'
	icon_state = "bandolier"
	item_state = "bandolier"
	storage_slots = 8
	max_combined_w_class = 20
	can_hold = list("/obj/item/ammo_casing/shotgun")
